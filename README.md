docker pull hashicorp/packer
source ../../../credentials
docker run -it --rm --entrypoint sh -v ${PWD}:/data -w /data hashicorp/packer

export AWS_ACCESS_KEY_ID="<tu key ID>"
export AWS_SECRET_ACCESS_KEY="<tu secreto>"
export AWS_DEFAULT_REGION="eu-west-1"
